import { pathToString, pathToId, rangeToString } from "@/core/path.js";
export function booqHref(booqId, path) {
    return path?.length
        ? `/booq/${booqId}/path/${pathToString(path)}#${pathToId(path)}`
        : `/booq/${booqId}/path/0`;
}

export function quoteRef(booqId, range) {
    return `/booq/${booqId}/quote/${rangeToString(range)}`;
}

export function feedHref() {
    return '/';
}