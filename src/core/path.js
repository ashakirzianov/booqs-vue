const idPrefix = 'path:';
export function pathToId(path) {
    return `${idPrefix}${pathToString(path)}`;
}

const pathSeparator = '-';
export function pathToString(path) {
    return path.join(pathSeparator);
}

export function pathFromString(pathString) {
    const path = pathString
        .split(pathSeparator)
        .map(c => parseInt(c, 10));
    return path.some(isNaN)
        ? undefined
        : path;
}

export function samePath(first, second) {
    return first.length === second.length
        && first.every((p, idx) => p === second[idx]);
}

export function pathLessThan(first, second) {
    const [firstHead, ...firstTail] = first;
    const [secondHead, ...secondTail] = second;
    if (secondHead === undefined) {
        return false;
    } else if (firstHead === undefined) {
        return true;
    } else if (firstHead === secondHead) {
        return pathLessThan(firstTail, secondTail);
    } else {
        return firstHead < secondHead;
    }
}

export function comparePaths(first, second) {
    return pathLessThan(first, second) ? -1
        : samePath(first, second) ? 0
            : +1;
}

export function pathInRange(path, range) {
    return pathLessThan(path, range.start)
        ? false
        : (
            range.end
                ? pathLessThan(path, range.end)
                : true
        );
}

const rangeSeparator = 'to';
export function rangeToString(range) {
    return range.end
        ? `${pathToString(range.start)}${rangeSeparator}${pathToString(range.end)}`
        : pathToString(range.start);
}

export function rangeFromString(rangeString) {
    const [startPart, endPart] = rangeString.split(rangeSeparator);
    const start = startPart !== undefined ? pathFromString(startPart) : undefined;
    const end = endPart !== undefined ? pathFromString(endPart) : undefined;
    return start && end
        ? { start, end }
        : undefined;
}

export function isOverlapping(first, second) {
    if (pathLessThan(first.start, second.start)) {
        if (first.end) {
            return pathLessThan(second.start, first.end);
        } else {
            return true;
        }
    } else {
        if (second.end) {
            return pathLessThan(first.start, second.end);
        } else {
            return true;
        }
    }
}