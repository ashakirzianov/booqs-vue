import Vue from 'vue'
import VueRouter from 'vue-router'
import { pathFromString, rangeFromString } from '../core/path'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },
  {
    path: '/booq/:source/:id/path/:path',
    name: 'BooqPath',
    component: () => import(/* webpackChunkName: "booq" */ '../views/Booq.vue'),
    props(route) {
      const path = pathFromString(route.params.path);
      return {
        booqId: `${route.params.source}/${route.params.id}`,
        path,
      };
    },
  },
  {
    path: '/booq/:source/:id/quote/:range',
    name: 'BooqQuote',
    component: () => import(/* webpackChunkName: "booq" */ '../views/Booq.vue'),
    props(route) {
      const range = rangeFromString(route.params.range);
      return {
        booqId: `${route.params.source}/${route.params.id}`,
        path: range.start,
        quote: range,
      };
    },
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
